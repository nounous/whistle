# Whistle
whistle est un script réalisé pour avertir les utilisateur⋅ices s'iels ont une
session qui dure depuis trop longtemps sur une machine.

La liste des machines à query est générée depuis le ldap. Le script se ssh
ensuite sur chaque machine pour faire tourner la commande `who` dessus. Si une
session est ouverte depuis plus d'un certain temps, un mail est envoyé.

## Deploiement

  1. Cloner le script dans `${HOME}/.local/services/`
  2. Copier la configuration exemple et adapter là à vos besoins
  ```bash
  $ cp whistle.example.json whistle.json
  $ edit whistle.json
  ```
  3. Lier le service et le timer systemd
  ```bash
  $ systemctl --user link ${PWD}/whistle.{service,timer}
  ```
  4. Démarrer le timer
  ```bash
  $ systemctl --user enable --now whistle.timer
  ```
