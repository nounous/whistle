#!/usr/bin/env python3
import argparse
from datetime import datetime
import email.message
import json
import os
import re
import smtplib
import subprocess
import sys

import jinja2
import ldap


path = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Notify light headed users about their unclosed sessions')
	args = parser.parse_args()
	with open(os.path.join(path, 'whistle.json')) as file:
		config = json.load(file)

	base = ldap.initialize(config['ldap']['server'])
	if config['ldap']['server'].startswith('ldaps://'):
		base.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_ALLOW)
		base.set_option(ldap.OPT_X_TLS_NEWCTX, 0)

	servers_id = base.search('ou=hosts,dc=crans,dc=org', ldap.SCOPE_SUBTREE, 'objectClass=ipHost')
	servers = base.result(servers_id)[1]
	servers = [ { k: list(map(lambda s: s.decode('utf8'),v)) for k,v in d.items() }
		for _,d in servers ]
	servers = [ server['cn'][0] for server in servers if server['cn'][0].endswith('adm.crans.org') ]

	whistle = {}
	for server in servers:
		try:
			who = subprocess.run(['ssh', server, 'who'], capture_output=True, timeout=10)
		except subprocess.TimeoutExpired:
			print(server)
			continue
		who = who.stdout.decode('utf8').strip().split('\n')
		regex = re.compile(r'_(?P<pseudo>[^\s]*)\s.*\s(?P<date>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2})')
		dates = [
			(match.group('pseudo'), datetime.strptime(match.group('date'), '%Y-%m-%d %H:%M'))
				for match in filter(lambda x: x is not None, map(regex.search,who))
		]
		now = datetime.now()
		s = {
			pseudo for pseudo, date in dates
				if (now - date).seconds > config['threshold']*3600
		}
		if len(s) > 0:
			whistle[server] = s

	if len(whistle) > 0:
		msg = email.message.EmailMessage()
		with open(os.path.join(path, 'templates/mail.j2')) as file:
			template = jinja2.Template(file.read())
		content = template.render(whistle=whistle, threshold=config['threshold']).strip()
		msg.set_content(content)
		msg['From'] = config['headers']['From']
		msg['To'] = config['headers']['To']
		msg['Subject'] = config['headers']['Subject']


		with smtplib.SMTP('172.16.10.124') as smtp:
			smtp.send_message(msg)
